const express = require("express");
const routes = express.Router();
const multer = require('multer');

const uploadConfig = require('../config/upload');
const uploads = multer(uploadConfig);

const user = require("./../controllers/UserController");
const procedure = require("./../controllers/ProcedureController");
const consultations = require("./../controllers/ConsultationController");

routes.get("/users/:id", user.show);
routes.get("/users", user.index);
routes.post("/users", uploads.single('userphoto'), user.store);
routes.delete("/users/:id", user.delete);

routes.get("/procedures/:id", procedure.show);
routes.get("/procedures", procedure.index);
routes.post("/procedures", procedure.store);
routes.delete("/procedures/:id", procedure.delete);

routes.get("/consultations/:id", consultations.show);
routes.get("/consultations", consultations.index);
routes.post("/consultations", consultations.store);
routes.delete("/consultations/:id", consultations.delete);
routes.get("/consultations/:id/report", consultations.report);


module.exports = routes;
