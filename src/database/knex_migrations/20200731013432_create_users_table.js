exports.up = function (knex) {
  return knex.schema.createTable("users", function (table) {
    table.increments();
    table.string("name").notNullable();
    table.datetime("birthday").notNullable();
    table.bigInteger("phone").notNullable();
    table.string("email").notNullable();
    table.string("photo");
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("users");
};
