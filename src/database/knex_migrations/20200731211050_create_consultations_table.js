exports.up = function (knex) {
  return knex.schema.createTable("consultations", function (table) {
    table.increments();
    table.integer('user_id').notNullable();
    table.foreign('user_id').references('id').inTable('users');
    table.string("complaint");
    table.integer("duration").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("consultations");
};
