exports.up = function (knex) {
  return knex.schema.createTable("procedures", function (table) {
    table.increments();
    table.string("name").notNullable();
    table.bigInteger("price").notNullable();
    table.integer("average_duration").notNullable();
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("procedures");
};
