exports.up = function (knex) {
  return knex.schema.createTable("consultation_has_procedures", function (table) {
    table.increments();
    table.integer('consultation_id').notNullable();
    table.foreign('consultation_id').references('id').inTable('consultations');
    table.integer('procedure_id').notNullable();
    table.foreign('procedure_id').references('id').inTable('procedures');
  });
};

exports.down = function (knex) {
  return knex.schema.dropTable("consultation_has_procedures");
};
