const knex = require("../database/connection");

module.exports = {
  //retorna todos os procedimentos cadastrados no sistema.
  async index(req, res) {
    const results = await knex("procedures").select("*");
    return res.json(results);
  },
  //retorna os dados de um procedimento em específico
  async show(req, res) {
    const { id } = req.params;
    try {
      const results = await knex("procedures").select("*").where("id", id);
      return res.json(results);
    } catch (error) {
      res.status(404).send({ message: "Procedure not found", error: error });
    }
  },

  //cria um procedimento
  async store(req, res) {
    const { name } = req.body;
    const exists = await knex("procedures").select("*").where("name", name);
    if (exists.length === 0) {
      try {
        const result = await knex("procedures").insert(req.body).returning("*");
        return res.status(201).send(result);
      } catch (error) {
        res
          .status(400)
          .send({ message: "Check the information sent", error: error });
      }
    } else {
      return res.status(409).send({ message: "Procedure already exists" });
    }
  },

  async delete(req, res) {
    try {
      const { id } = req.params;
      await knex("procedures").where("id", id).del();
      return res.status(200).send();
    } catch (error) {
      return res.status(400).send();
    }
  },
};
