const knex = require("../database/connection");

module.exports = {
  //retorna todos os usuários cadastrados no sistema.
  async index(req, res) {
    const results = await knex("users").select("*");
    return res.json(results);
  },
  //retorna os dados de um usuário em específico
  async show(req, res) {
    const { id } = req.params;

    try {
      const results = await knex("users").select("*").where("id", id);
      return res.json(results);
    } catch (error) {
      res.status(404).send({ message: "User not found", error: error });
    }
  },

  //cria um usuário
  async store(req, res) {
    let img;
    const { name, email, birthday, phone } = req.body;

    if(req.file){
      img = req.file.filename
    }

    const exists = await knex("users").select("*").where("email", email);
    if (exists.length === 0) {
      try {
        const result = await knex("users")
          .insert({
            name,
            email,
            birthday,
            phone,
            photo: img || null,
          })
          .returning("*");
        return res.status(201).send(result);
      } catch (error) {
        res
          .status(400)
          .send({ message: "Check the information sent", error: error });
      }
    } else {
      return res.status(409).send({ message: "User already exists" });
    }
  },

  async delete(req, res) {
    try {
      const { id } = req.params;
      await knex("users").where("id", id).del();
      return res.status(200).send();
    } catch (error) {
      return res.status(400).send();
    }
  },
};
