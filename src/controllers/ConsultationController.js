const knex = require("../database/connection");

module.exports = {
  //retorna todas as consultas cadastrados no sistema.
  async index(req, res) {
    try {
      const result = await knex("consultations").select("*");
      return res.json(result);
    } catch (error) {
      res.status(400).send({ error });
    }
    return res.json(results);
  },
  //retorna os dados de uma consulta em específica
  async show(req, res) {
    const { id } = req.params;
    try {
      const response = await knex("consultation_has_procedures")
        .where("consultation_id", id)
        .join(
          "consultations",
          "consultation_has_procedures.consultation_id",
          "consultations.id"
        )
        .join(
          "procedures",
          "consultation_has_procedures.procedure_id",
          "procedures.id"
        )
        .join(
          "users",
          "consultations.user_id",
          "users.id"
        )
        .select(
          "users.name as user_name",
          "consultation_id",
          "consultations.duration AS consultation_duration",
          "consultations.complaint AS consultation_complaint",

          "procedures.name AS procedure_name",
          "procedures.price AS procedure_price",
          "procedures.average_duration AS procedure_average_duration"
        );

      const result = {
        consultation_id: response[0].consultation_id,
        user_name: response[0].user_name,
        consultation_duration: response[0].consultation_duration,
        consultation_complaint: response[0].consultation_complaint,
        procedures: [],
      };

      response.map((item) => {
        result.procedures.push({
          procedure_name: item.procedure_name,
          procedure_price: item.procedure_price,
          procedure_average_duration: item.procedure_average_duration,
        });
      });

      return res.json(result);
    } catch (error) {
      res.status(404).send({ message: "Consultation not found", error: error });
    }
  },


  async report(req, res) {
    const { id } = req.params;
    try {
      const response = await knex("consultation_has_procedures")
        .where("consultation_id", id)
        .join(
          "consultations",
          "consultation_has_procedures.consultation_id",
          "consultations.id"
        )
        .join(
          "procedures",
          "consultation_has_procedures.procedure_id",
          "procedures.id"
        )
        .join(
          "users",
          "consultations.user_id",
          "users.id"
        )
        .select(
          "users.name as user_name",
          "users.phone as user_phone",
          "users.email as user_email",
          "users.photo as user_photo",
          "users.birthday as user_birthday",
          "consultation_id",
          "consultations.duration AS consultation_duration",
          "consultations.complaint AS consultation_complaint",

          "procedures.name AS procedure_name",
          "procedures.price AS procedure_price",
          "procedures.average_duration AS procedure_average_duration"
        );

      const result = {
        consultation_id: response[0].consultation_id,
        consultation_duration: response[0].consultation_duration,
        consultation_complaint: response[0].consultation_complaint,
        procedures: [],
        user: [],
        totalPrice: 0,
        totalTime: 0,
      };

      response.map((item) => {
        result.procedures.push({
          procedure_name: item.procedure_name,
          procedure_price: item.procedure_price,
          procedure_average_duration: item.procedure_average_duration,
        });
        result.totalPrice = result.totalPrice + parseInt(item.procedure_price)
        result.totalTime =  result.totalTime + item.procedure_average_duration
      });

      result.user.push({
        user_name: response[0].user_name,
        user_phone: response[0].user_phone,
        user_photo: response[0].user_photo,
        user_email: response[0].user_email,
        user_birthday: response[0].user_birthday,
      });

      return res.json(result);
    } catch (error) {
      res.status(404).send({ message: "Consultation not found", error: error });
    }
  },

  //cria uma consulta
  async store(req, res) {
    const { duration, user_id, complaint, procedures } = req.body;

    try {
      const consultationQuery = await knex("consultations")
        .insert({
          duration,
          user_id,
          complaint,
        })
        .returning("id");
      procedures.map(async (procedure) => {
        return await knex("consultation_has_procedures").insert({
          consultation_id: consultationQuery[0],
          procedure_id: procedure,
        });
      });
      return res.status(201).send();
    } catch (error) {
      res
        .status(400)
        .send({ message: "Check the information sent", error: error });
    }
  },

  async delete(req, res) {
    try {
      const { id } = req.params;
      await knex("consultations").where("id", id).del();
      return res.status(200).send();
    } catch (error) {
      return res.status(400).send();
    }
  },
};
