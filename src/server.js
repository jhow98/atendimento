const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const path = require('path');

const app = express()

app.use('/files', express.static(path.resolve(__dirname, 'files')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", '*');
  res.header("Access-Control-Allow-Credentials", true);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
  next();
});

app.use(cors());
app.use(express.json());
app.use(routes);


app.listen(process.env.PORT || 3333, () =>  console.log('Server is running'));